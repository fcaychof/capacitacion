package com.nova.consultoria.controller;

import com.nova.consultoria.model.Consultoria;
import com.nova.consultoria.service.ConsultoriaService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@AllArgsConstructor
@RequestMapping("/consultoria")
@Slf4j
public class ConsultoriaController {

    private ConsultoriaService service;

    @GetMapping
    public ResponseEntity<List<Consultoria>> findAll() {
        return ResponseEntity.ok(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Consultoria> findById(@PathVariable(value = "id") Long id) {
        return ResponseEntity.ok(service.findById(id));
    }

    @PutMapping
    public ResponseEntity<Consultoria> update(@RequestBody Consultoria consultoria) {
        return ResponseEntity.ok(service.save(consultoria));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        service.delete(id);
        return ResponseEntity.ok("Consultoria borrada com exito");
    }


    @PostMapping
    public ResponseEntity<Consultoria> add(@RequestBody Consultoria consultoria)  {
         return ResponseEntity.status(HttpStatus.CREATED).body(service.save(consultoria));
    }


}
