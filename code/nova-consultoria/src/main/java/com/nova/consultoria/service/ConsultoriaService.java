package com.nova.consultoria.service;

import com.nova.consultoria.model.Consultoria;
import com.nova.consultoria.repository.ConsultoriaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsultoriaService {

    private ConsultoriaRepository repository;

    public ConsultoriaService(ConsultoriaRepository repository) {
        this.repository = repository;
    }

    public List<Consultoria> listar() {
        return repository.findAll();
    }

    public Consultoria findById(Long id) {
        return repository.findById(id).get();
    }

    public Consultoria save(Consultoria consultoria) {
        return repository.save(consultoria);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

}
