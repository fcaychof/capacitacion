package com.nova.consultoria.repository;

import com.nova.consultoria.model.Consultoria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ConsultoriaRepository extends JpaRepository<Consultoria, Long> {
}
