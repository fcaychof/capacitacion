package com.novatronic.norma.controller;

import com.novatronic.norma.model.Norma;
import com.novatronic.norma.service.NormaService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
@RequestMapping("/norma")
@Slf4j
public class NormaController {

    private NormaService service;

    @GetMapping
    public ResponseEntity<List<Norma>> findAll() {
        return ResponseEntity.ok(service.listar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Norma> findById(@PathVariable(value = "id") Integer id) {
        return ResponseEntity.ok(service.findById(id));
    }

    @PutMapping
    public ResponseEntity<Norma> update(@RequestBody Norma norma) {
        service.findById(norma.getId());

        return ResponseEntity.ok(service.save(norma));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Norma> desabilitar(@PathVariable(value = "id") Integer id) {
        Norma norma = service.findById(id);

        if("ATIVA".equals(norma.getStatus())){
            norma.setStatus("INATIVA");
        }else{
            norma.setStatus("ATIVA");
        }
        return ResponseEntity.ok(service.save(norma));
    }


    @PostMapping
    public ResponseEntity<Norma> add(@RequestBody Norma norma) throws Exception {
     try {
         return ResponseEntity.status(HttpStatus.CREATED).body(service.save(norma));
     }
         catch (Exception ex) {
            log.error("Erro al momento de guardar una nueva norma ", ex);
             return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(norma);
         }
        }



}
