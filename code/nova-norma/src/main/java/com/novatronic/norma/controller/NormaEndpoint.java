package com.novatronic.norma.controller;

import com.novatronic.norma.service.NormaService;
import io.spring.guides.gs_producing_web_service.GetNormaRequest;
import io.spring.guides.gs_producing_web_service.GetNormaResponse;
import lombok.AllArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@AllArgsConstructor
public class NormaEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
	private NormaService service;


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getNormaRequest")
	@ResponsePayload
	public GetNormaResponse getNorma(@RequestPayload GetNormaRequest request) {
		GetNormaResponse response = new GetNormaResponse();
		response.setNorma(service.convert(service.findById(request.getId())));
		return response;
	}
}
