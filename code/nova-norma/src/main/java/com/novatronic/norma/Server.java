package com.novatronic.norma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com")
@EntityScan("com.novatronic.norma.model")
@EnableJpaRepositories("com.novatronic.norma.repository")
public class Server extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(Server.class, args);
    }

}


