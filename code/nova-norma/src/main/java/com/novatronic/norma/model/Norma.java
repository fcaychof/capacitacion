package com.novatronic.norma.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "tb_norma")
public class Norma {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "codigoNorma")
    private String codigoNorma;

    @Column(name = "tipoDocumento")
    private String tipoDocumento;

    @Column(name = "orgaoExpedidor")
    private String orgaoExpedidor;

    @Column(name = "dataPublicacao")
    private String dataPublicacao;

    @Column(name = "dataVigencia")
    private String dataVigencia;

    @Column(name = "resolucao")
    private String resolucao;

    @Column(name = "nome")
    private String nomeTecnico;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "status")
    private String status;

    @Column(name = "tipo")
    private String tipoNorma;

    @Column(name = "area")
    private String areaDeNegocio;

    @Column(name = "periodoContratacao")
    private String periodoContratacao;

    @Column(name = "nomeFile")
    private String nomeFile;

    @Column(name = "departamento")
    private String departamento;

    @Column(name = "urlLink")
    private String urlLink;
}
