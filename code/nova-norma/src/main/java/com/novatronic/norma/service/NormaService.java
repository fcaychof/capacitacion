package com.novatronic.norma.service;

import com.novatronic.norma.model.Norma;
import com.novatronic.norma.repository.NormaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NormaService {

    private final NormaRepository repository;

    public NormaService(NormaRepository repository) {
        this.repository = repository;
    }

    public List<Norma> listar() {
        return repository.findAll();
    }

    public Norma findById(Integer id) {
        return repository.findById(id).get();
    }

    public io.spring.guides.gs_producing_web_service.Norma convert(Norma byId) {
        io.spring.guides.gs_producing_web_service.Norma norma = new io.spring.guides.gs_producing_web_service.Norma();
        norma.setCodigoNorma(byId.getCodigoNorma());
        norma.setTipoDocumento(byId.getTipoDocumento());
        norma.setOrgaoExpedidor(byId.getOrgaoExpedidor());
        norma.setDataPublicacao(byId.getDataPublicacao());
        norma.setDataVigencia(byId.getDataVigencia());
        norma.setResolucao(byId.getResolucao());
        norma.setNomeTecnico(byId.getNomeTecnico());
        norma.setDescricao(byId.getDescricao());
        norma.setStatus(byId.getStatus());
        norma.setTipoNorma(byId.getTipoNorma());
        norma.setAreaDeNegocio(byId.getAreaDeNegocio());
        norma.setPeriodoContratacao(byId.getPeriodoContratacao());
        norma.setNomeFile(byId.getNomeFile());
        norma.setDepartamento(byId.getDepartamento());
        norma.setUrlLink(byId.getUrlLink());
        norma.setId(byId.getId());

        return norma;

    }

    public Norma save(Norma norma) {
        return repository.save(norma);
    }

}
